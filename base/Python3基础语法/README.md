# Python3 基础语法

- [注释](#注释)
- [行与缩进](#行与缩进)
- [多行语句](#多行语句)
- [数字(Number)类型](#数字(Number)类型)
- [字符串(String)](#字符串(String))
- [空行](#空行)
- [等待用户输入](#等待用户输入)
- [同一行显示多条语句](#同一行显示多条语句)
- [多个语句构成代码组](#多个语句构成代码组)
- [print 输出](#print输出)
- [import 与 from...import](#import与fromimport)



## 注释

&emsp;&emsp;`Python` 中单行注释以 `#` 开头，实例如下：

```python
# 第一个注释
print ("Hello, Python!") 
# 第二个注释
print ("Hello, Waffle Nano!")
```

执行以上代码，输出结果为：

<center><img src="assets/image/1.png" width="500"/></center>

<center>(注释)</center>

&emsp;

多行注释可以用多个 `#` 号，还有 `'''` 和 `"""`：

```python3
# 第一个注释
# 第二个注释
 
'''
第三注释
第四注释
'''
 
"""
第五注释
第六注释
"""
print ("Hello, Python!")
```

执行以上代码，输出结果为：

<center><img src="assets/image/2.png" width="500"/></center>

<center>(多行注释)</center>

&emsp;

---

## 行与缩进

&emsp;&emsp;`python` 最具特色的就是使用缩进来表示代码块，不需要使用大括号` {} `。

&emsp;&emsp;缩进的 **空格数** 是可变的，但是同一个代码块的语句必须包含 **相同** 的缩进空格数。实例如下： 

```python
if True:
    print ("True")
else:
    print ("False")
```

&emsp;&emsp;以下代码最后一行语句 **缩进数** 的空格数不一致，会导致运行错误：

```python
if True:
    print ("Answer")
    print ("True")
else:
    print ("Answer")
  print ("False")    # 缩进不一致，会导致运行错误
```

&emsp;&emsp;以上程序由于缩进不一致，执行后会出现类似以下错误：


<center><img src="assets/image/3.png" width="500"/></center>

<center>(多行注释)</center>

&emsp;

---

## 多行语句

&emsp;&emsp;`Python` 通常是一行写完一条语句，但如果语句很长，我们可以使用反斜杠 `\` 来实现多行语句，例如：

```python
total = item_one + \
        item_two + \
        item_three
```

&emsp;&emsp;在 `[]`, `{}`, 或 `()` 中的多行语句，不需要使用反斜杠 `\`，例如：

```python
total = ['item_one', 'item_two', 'item_three',
        'item_four', 'item_five']
```

---

## 数字(Number)类型

&emsp;&emsp;`python` 中数字有四种类型：**整数**、**布尔型**、**浮点数** 和**复数**。

- **int** (整数), 如 1, 只有一种整数类型 `int`，表示为长整型，没有 `python2` 中的 `Long`；

- **bool** (布尔), 如 **True**；

- **float** (浮点数), 如 **1.23**、**3E-2**；

- **complex** (复数), 如 **1 + 2j**、 **1.1 + 2.2j**；

---

## 字符串(String)

- `python` 中单引号和双引号使用完全相同；

- 使用三引号(`'''` 或 `"""`)可以指定一个多行字符串；

- 转义符 `\` ；

- 反斜杠可以用来转义，使用r可以让反斜杠不发生转义。。 如 `r"this is a line with \n"` 则 `\n` 会显示，并不是换行。

- 按字面意义级联字符串，如`"this "` `"is "` `"string"` 会被自动转换为`this is string`。

- 字符串可以用 `+` 运算符连接在一起，用 `*` 运算符重复；

- `Python` 中的字符串有两种索引方式，从左往右以 `0` 开始，从右往左以 `-1` 开始；

- `Python` 中的字符串不能改变；

- `Python` 没有单独的字符类型，一个字符就是长度为 `1` 的字符串；

- 字符串的截取的语法格式如下：`变量[头下标:尾下标]`，开发板中的 python 在对字符操作的时候不支持 `变量[头下标:尾下标:步长]` 这样的操作。

```python
word = '字符串'
sentence = "这是一个句子。"
paragraph = """这是一个段落，
可以由多行组成"""
```

```python
str='123456789'
 
print(str)                 # 输出字符串
print(str[0:-1])           # 输出第一个到倒数第二个的所有字符
print(str[0])              # 输出字符串第一个字符
print(str[2:5])            # 输出从第三个开始到第五个的字符
print(str[2:])             # 输出从第三个开始后的所有字符
print(str * 2)             # 输出字符串两次
print(str + '你好')         # 连接字符串
 
print('------------------------------')
 
print('hello\nWaffleNano')      # 使用反斜杠(\)+n转义特殊字符
print(r'hello\nWaffleNano')     # 在字符串前面添加一个 r，表示原始字符串，不会发生转义
```

&emsp;&emsp;这里的 `r` 指 `raw`，即 `raw string`，会自动将反斜杠转义，例如：

```shell
>>> print('\n')       # 输出空行
>>> print(r'\n')      # 输出 \n
>>> \n
```

以上实例输出结果：

<center><img src="assets/image/4.png" width="500"/></center>

<center>(字符串操作)</center>

&emsp;

---

## 空行

&emsp;&emsp;函数之间或类的方法之间用空行分隔，表示一段新的代码的开始。类和函数入口之间也用一行空行分隔，以突出函数入口的开始。

&emsp;&emsp;空行与代码缩进不同，空行并不是 `Python` 语法的一部分。书写时不插入空行，`Python` 解释器运行也不会出错。但是空行的作用在于分隔两段不同功能或含义的代码，便于日后代码的维护或重构。

&emsp;&emsp;记住：空行也是程序代码的一部分。

---

## 等待用户输入

&emsp;&emsp;执行下面的程序在按回车键后就会等待用户输入：

```python
input("\n\n按下 enter 键后退出。")
```

&emsp;&emsp;以上代码中 ， `"\n\n"` 在结果输出前会输出两个新的空行。一旦用户按下 `enter` 键时，程序将退出。

<center><img src="assets/image/5.png" width="500"/></center>

<center>(input输入测试)</center>

&emsp;

---

## 同一行显示多条语句

&emsp;&emsp;`Python` 可以在同一行中使用多条语句，语句之间使用分号 `;` 分割，以下是一个简单的实例：

```python
print("123");print("456")
```

使用脚本执行以上代码，输出结果为：

<center><img src="assets/image/6.png" width="500"/></center>

<center>(同一行显示多条语句)</center>

&emsp;

---

## 多个语句构成代码组

&emsp;&emsp;缩进相同的一组语句构成一个代码块，我们称之代码组。

&emsp;&emsp;像 `if` 、 `while` 、`def` 和 `class` 这样的复合语句，首行以关键字开始，以冒号 `( : )` 结束，该行之后的一行或多行代码构成代码组。

&emsp;&emsp;我们将首行及后面的代码组称为一个子句`(clause)`。

如下实例：

```python
if expression : 
   suite
elif expression : 
   suite 
else : 
   suite
```

---

## print输出

&emsp;&emsp;`print` 默认输出是换行的，如果要实现不换行需要在变量末尾加上 `end=""`：

```python
x="a"
y="b"
# 换行输出
print( x )
print( y )
 
print('---------')
# 不换行输出
print( x, end=" " )
print( y, end=" " )
print()
```
以上实例执行结果为：

<center><img src="assets/image/7.png" width="500"/></center>

<center>(print 输出)</center>

&emsp;

---

## import与from...import

&emsp;&emsp;在 `python` 用 `import` 或者 `from...import` 来导入相应的模块。

&emsp;&emsp;将整个模块 `(somemodule)` 导入，格式为： `import somemodule`

&emsp;&emsp;从某个模块中导入某个函数,格式为： `from somemodule import somefunction`

&emsp;&emsp;从某个模块中导入多个函数,格式为： `from somemodule import firstfunc, secondfunc, thirdfunc`

&emsp;&emsp;将某个模块中的全部函数导入，格式为： `from somemodule import *`

```python
import math
print('================Python import mode==========================')
print ('数学中的Π为:')
print (math.pi)
```

<center><img src="assets/image/8.png" width="500"/></center>

<center>(import 导入)</center>

&emsp;

---