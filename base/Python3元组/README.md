# Python3元组

- [访问元组](#访问元组)
- [修改元组](#修改元组)
- [删除元组](#删除元组)
- [元组运算符](#元组运算符)
- [元组索引，截取](#元组索引截取)
- [元组内置函数](#元组内置函数)

&emsp;&emsp;`Python` 的 **元组** 与 **列表** 类似，不同之处在于 **元组** 的元素不能修改。

&emsp;&emsp;**元组** 使用小括号 `( )`，**列表** 使用方括号 `[ ]`。

&emsp;&emsp;元组创建很简单，只需要在括号中添加元素，并使用逗号隔开即可。

&emsp;&emsp;实例演示：

```shell
>>> tup1 = ('Google', 'Waffle', 1997, 2000)
>>> tup2 = (1, 2, 3, 4, 5 )
>>> tup3 = "a", "b", "c", "d"   #  不需要括号也可以
>>> type(tup3)
<class 'tuple'>
```

&emsp;&emsp;创建空元组:

```python
tup1 = ()
```

&emsp;&emsp;元组中只包含一个元素时，需要在元素后面添加逗号 `,` ，否则括号会被当作运算符使用：

```shell
>>> tup1 = (50)
>>> type(tup1)     # 不加逗号，类型为整型
<class 'int'>

>>> tup1 = (50,)
>>> type(tup1)     # 加上逗号，类型为元组
<class 'tuple'>
```

&emsp;&emsp;元组与字符串类似，下标索引从 `0` 开始，可以进行截取，组合等。

```shell
>>> tup1 = ('Google', 'Waffle', 1997, 2000)
>>> print(tup1[1])
Waffle
```

---

## 访问元组

&emsp;&emsp;元组可以使用下标索引来访问元组中的值，如下实例:

```python
tup1 = ('Google', 'Waffle', 1997, 2000)
tup2 = (1, 2, 3, 4, 5, 6, 7 )
 
print ("tup1[0]: ", tup1[0])
print ("tup2[1:5]: ", tup2[1:5])
```

以上实例输出结果：

```shell
tup1[0]:  Google
tup2[1:5]:  (2, 3, 4, 5)
```

---

## 修改元组

&emsp;&emsp;元组中的元素值是不允许修改的，但我们可以对元组进行连接组合，如下实例:

```python
tup1 = (12, 34.56)
tup2 = ('abc', 'xyz')
 
# 以下修改元组元素操作是非法的。
# tup1[0] = 100
 
# 创建一个新的元组
tup3 = tup1 + tup2
print (tup3)
```

&emsp;&emsp;以上实例输出结果：

```
(12, 34.56, 'abc', 'xyz')
```
---

## 删除元组

&emsp;&emsp;元组中的元素值是 **不允许** 删除的，但我们可以使用 `del` 语句来删除整个元组，如下实例:

```python
tup = ('Google', 'Waffle', 1997, 2000)
 
print (tup)
del tup
print ("删除后的元组 tup : ")
print (tup)
```

&emsp;&emsp;以上实例元组被删除后，输出变量会有异常信息，输出如下所示：

```shell
删除后的元组 tup : 
Traceback (most recent call last):
  File "test.py", line 6, in <module>
    print (tup)
NameError: name 'tup' is not defined
```

---

## 元组运算符

&emsp;&emsp;与字符串一样，元组之间可以使用 `+` 号和 `*` 号进行运算。这就意味着他们可以组合和复制，运算后会生成一个新的元组。

| Python 表达式 | 结果 | 描述 |
| --- | --- | --- |
| len((1, 2, 3)) | 3 | 计算元素个数 |
| (1, 2, 3) + (4, 5, 6) | (1, 2, 3, 4, 5, 6) | 连接 |
| ('Hi!',) * 4  | ('Hi!', 'Hi!', 'Hi!', 'Hi!') | 复制 | 
| 3 in (1, 2, 3) | True | 元素是否存在 |
| for x in (1, 2, 3): print (x,) | 1 2 3 | 	迭代  |

---

## 元组索引，截取

&emsp;&emsp;因为元组也是一个序列，所以我们可以访问元组中的指定位置的元素，也可以截取索引中的一段元素，如下所示：

元组：

```python
tup = ('Google', 'Waffle', 'Taobao', 'Wiki', 'Weibo','Weixin')
```

| Python 表达式 | 结果 | 描述 |
| --- | --- | --- |
| tup[1] | 'Waffle' | 读取第二个元素 |
| tup[-2] | 'Weibo' | 反向读取，读取倒数第二个元素 |
| tup[1:] | ('Waffle', 'Taobao', 'Wiki', 'Weibo', 'Weixin') | 截取元素，从第二个开始后的所有元素。|
| tup[1:4] | ('Waffle', 'Taobao', 'Wiki') | 截取元素，从第二个开始到第四个元素（索引为 3）。|

---

## 元组内置函数

&emsp;&emsp;`Python` 元组包含了以下内置函数

| 方法 | 描述 |
| --- | --- | 
| len(tuple) | 计算元组元素个数 |
| max(tuple) | 返回元组中元素最大值。注：元组内必须都是数字类型 |
| min(tuple) | 返回元组中元素最小值。注：元组内必须都是数字类型 |
| tuple(iterable) | 将可迭代系列转换为元组。 |

**关于元组是不可变的**

&emsp;&emsp;所谓元组的不可变指的是 **元组所指向的 内存 中的 内容 不可变**。

```shell
>>> tup = ('W', 'a', 'f', 'f', 'l', 'e')
>>> tup[0] = 'g'     # 不支持修改元素
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
>>> id(tup)     # 查看内存地址
4440687904
>>> tup = (1,2,3)
>>> id(tup)
4441088800    # 内存地址不一样了
```

&emsp;&emsp;从以上实例可以看出，重新赋值的元组 `tup`，绑定到新的对象了，不是修改了原来的对象。

