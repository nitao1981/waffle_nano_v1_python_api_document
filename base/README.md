# 支持的 Python3 语法


&emsp;&emsp;`Python` 是一个高层次的结合了 **解释性**、**编译性**、**互动性** 和 **面向** 对象的脚本语言。

&emsp;&emsp;`Python` 的设计具有很强的可读性，相比其他语言经常使用英文关键字，其他语言的一些标点符号，它具有比其他语言更有特色语法结构。

## 目录

| 语法 | 说明 |
|---|---|
|[Python3 基础语法](Python3基础语法/README.md)|  python3 基础语法展示 |
|[Python3 基本数据类型](Python3基本数据类型/README.md)| Python3 中的六个标准的数据类型 |
|[Python3 注释](Python3注释/README.md)| 确保对模块, 函数, 方法和行内注释使用正确的风格 |
|[Python3 运算符](Python3运算符/README.md)| 本章节主要说明 Python 的运算符 |
|[Python3 数字(Number)](Python3数字(Number)/README.md)| Python 数字数据类型用于存储数值。 |
|[Python3 字符串](Python3字符串/README.md)| 字符串是 Python 中最常用的数据类型。 |
|[Python3 列表](Python3列表/README.md)| 序列是 Python 中最基本的数据结构。 |
|[Python3 元组](Python3元组/README.md)| Python 的元组与列表类似，不同之处在于元组的元素不能修改。 |
|[Python3 字典](Python3字典/README.md)| 字典是另一种可变容器模型，且可存储任意类型对象 |
|[Python3 集合](Python3集合/README.md)| 集合（set）是一个无序的不重复元素序列 |
|[Python3 条件控制](Python3条件控制/README.md)| Python3中的条件控制 |
|[Python3 循环语句](Python3循环语句/README.md)| Python3中的循环语句 |
|[Python3 迭代器与生成器](Python3迭代器与生成器/README.md)| Python3中的迭代器与生成器 |
|[Python3 函数](Python3函数/README.md)| 函数是组织好的，可重复使用的，用来实现单一，或相关联功能的代码段。 |
|[Python3 数据结构](Python3数据结构/README.md)| Python3 数据结构 |
|[Python3 模块](Python3模块/README.md)| Python3 模块 |
|[Python3 输入和输出](Python3输入和输出/README.md)| Python 的输入输出 |
|[Python3 错误和异常](Python3错误和异常/README.md)| Python 有两种错误很容易辨认：语法错误和异常 |
|[Python3 面向对象](Python3面向对象/README.md)| Python从设计之初就已经是一门面向对象的语言，正因为如此，在Python中创建一个类和对象是很容易的。 |

> 目前 Waffle Nano 鸿蒙固件已经支持 `python3.6` 的绝大多数语法，本手册基于 [菜鸟教程](https://www.runoob.com/python3/python3-tutorial.html) 归纳整理出已经支持的语法供开发者使用。