# math – 数学函数

## 概要

&emsp;&emsp;该模块实现相应CPython模块的子集

> `math` 模块提供一些用于处理浮点数的基本函数。

## math API详解

&emsp;&emsp;使用`import math`导入`math`模块

&emsp;&emsp;再使用`TAB` 按键来查看`math`中所包含的内容：

```python
>>> import math
>>> math.
__name__        pow             acos            asin
atan            atan2           ceil            copysign
cos             degrees         e               exp
fabs            floor           fmod            frexp
isfinite        isinf           isnan           ldexp
log             modf            pi              radians
sin             sqrt            tan             trunc
```

### 函数

- `math.acos`(*x*)

  返回 `x` 的反余弦函数。

- `math.acosh`(*x*)

  返回 `x` 的反双曲余弦函数。

- `math.asin`(*x*)

  返回 `x` 的反正弦函数。

- `math.asinh`(*x*)

  返回 `x` 的反双曲正弦函数。

- `math.atan`(*x*)

  返回 `x` 的反正切函数。

- `math.atan2`(*y*, *x*)

  返回 `y/x` 的反正切函数的主值。

- `math.atanh`(*x*)

  返回 `x` 的反双曲正切函数。

- `math.ceil`(*x*)

  返回一个正整数,将 `x` 向正无穷大舍入。

- `math.copysign`(*x*, *y*)

  以 `y` 的符号返回 `x` 。

- `math.cos`(*x*)

  返回 `x` 的的余弦函数。

- `math.cosh`(*x*)

  返回 `x` 的双曲余弦函数。

- `math.degrees`(*x*)

  返回弧度 `x` 对应的度数。

- `math.erf`(*x*)

  返回 `x` 的误差函数。

- `math.erfc`(*x*)

  返回 `x` 的补差函数。

- `math.exp`(*x*)

  返回 `x` 的指数。

- `math.expm1`(*x*)

  返回 `exp(x) - 1`.

- `math.fabs`(*x*)

  返回 `x` 的绝对值。

- `math.floor`(*x*)

  返回一个整数,将 `x` 向负无穷大舍入.

- `math.fmod`(*x*, *y*)

  返回 `x/y` 的余数。

- `math.frexp`(*x*)

  将浮点数分解为尾数和指数。返回值是元组 `(m, e)` ，确切表示为 $x = m * 2^e$ 。 若 `x == 0`，则该函数返回 `(0.0, 0)` ，否则 `0.5 <= abs(m) < 1` 关系成立。

- `math.gamma`(*x*)

  返回 `x` 的伽玛函数。

- `math.isfinite`(*x*)

  若 `x` 为有限的，则返回 `True` 。

- `math.isinf`(*x*)

  若 `x` 为无限，则返回 `True` 。

- `math.isnan`(*x*)

  若 `x` 非数字，则返回 `True` 。

- `math.ldexp`(*x*, *exp*)

  返回 `x * (2**exp)`.

- `math.lgamma`(*x*)

  返回 `x` 的伽玛函数的自然对数。

- `math.log`(*x*)

  返回 `x` 的自然对数。

- `math.log10`(*x*)

  返回 `x` 以10为底的对数。

- `math.log2`(*x*)

  返回 `x` 以2为底的对数。

- `math.modf`(*x*)

  返回包含两个浮点值的元组，即x的分数和积分部分。两个返回值都与 `x` 有同样标记。

- `math.pow`(*x*, *y*)

  将 `x` 返回 `y` 的幂。

- `math.radians`(*x*)

  返回度数 `x` 的弧度。

- `math.sin`(*x*)

  返回 `x` 的正弦函数。

- `math.sinh`(*x*)

  返回的 `x` 双曲正弦函数。

- `math.sqrt`(*x*)

  返回 `x` 的平方根.

- `math.tan`(*x*)

  返回 `x` 的正切函数。

- `math.tanh`(*x*)

  返回 `x` 的双曲正切函数。

- `math.trunc`(*x*)

  截尾函数，返回 `x` 的整数部分。

### 常量

- `math.e`

  自然对数的底。

- `math.pi`

  一个圆的周长与其直径的比值。