# bme280

## 简介

这是基于 `BME280` 传感器的驱动库，支持获取温度、湿度和气压数据。

## BME280 实例
----------------

要创建 `BME280` 实例，必须要有一个 `machine.I2C`

``` python
from machine import I2C
import bme280, utime

bme = bme280.BME280(I2C(0), filter=bme280.FILTER_16, temp_mode=bme280.SAMPLE_16X, hum_mode=bme280.SAMPLE_16X, pressure_mode=bme280.SAMPLE_16X)
utime.sleep(1)
```

- `BME280.temperature()`

  获取温度数据

- `BME280.humidity()`

  获取湿度数据

- `BME280.pressure()`

    获取气压数据

## 模块其他定义

- 采样频率定义：`SAMPLE_OFF`, `SAMPLE_1X`, `SAMPLE_2X`, `SAMPLE_4X`, `SAMPLE_8X`, `SAMPLE_16X`

- 过滤器定义：`FILTER_OFF`, `FILTER_2`, `FILTER_4`, `FILTER_8`, `FILTER_16`
