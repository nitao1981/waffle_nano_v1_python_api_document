# 定时器

  - [概要](#概要)
    - [什么是定时器？](#什么是定时器)
  - [machine.Timer API详解](#machinetimer-api详解)
    - [宏定义](#宏定义)
    - [类](#类)
      - [构造定时器](#构造定时器)
    - [函数](#函数)
      - [初始化](#初始化)
      - [释放资源](#释放资源)
      - [查看状态](#查看状态)
  - [示例](#示例)

## 概要

&emsp;&emsp; 本节介绍了定时器与回调函数的概念，以及如何在MicroPython中使用定时器。

### 什么是定时器？

&emsp;&emsp;定时器可以理解为一个闹钟，设定特定时间之后执行某件事情，也可以周期的执行某件事情。

&emsp;&emsp;定时器与`utime`模块中的`sleep`延时函数最大区别在于，延时函数期间，CPU就如同进入了睡眠，之后的事情只有等睡醒之后再做，这种情况我们称之为**阻塞**， 睡觉阻塞了cpu去完成其他的任务，必须等待CPU睡醒；而定时器是**非阻塞**的，在未到达定时器定时周期结束之前，CPU可以去做别的事情，等到定时器计时完毕，便会去通知CPU，CPU再去执行**回调函数** （在你设定闹钟之前，你决定要在闹钟响了之后去做的事情）。

&emsp;&emsp;定时器每个周期都会产生一次中断，然后调用特定的 **回调函数callback**, 定时器中断属于内部中断.

> #### 中断
>
> CPU的中断分为很多种，不同的中断拥有不同的优先级别。当多个中断同时发生时，计算机按照优先级来以此处理

## machine.Timer API详解

&emsp;&emsp;使用`import Timer`导入定时器`Timer`模块

&emsp;&emsp;再使用`TAB` 按键来查看`Timer`中所包含的内容：

```python
>>> from machine import Timer
>>> Timer.
__del__         ONCE            PERIODIC        deinit
init            isrunning
```

### 宏定义

&emsp;&emsp;下面的宏定义用于配置network，设置对应网络模式。

| 宏定义         | 含义                                       |
| -------------- | :----------------------------------------- |
| Timer.ONCE     | 计时器运行一次，直到通道的配置时间到期为止 |
| Timer.PERIODIC | 计时器以通道的已配置频率定期运行           |

### 类

&emsp;&emsp;`class Time(id)`

&emsp;&emsp;`id`：任意正整数作为ID

#### 构造定时器

&emsp;&emsp;用Timer对象的构造器函数

&emsp;&emsp;示例：

```python
>>> from machine import Timer
>>> timer = Timer(1)#创建一个定时器
```

### 函数

&emsp;&emsp;在接下来的示例中, 构造`id=1`的`Timer`对象来列举`Timer`对象的函数。

```python
>>> from machine import Timer
>>> timer = Timer(1)#创建一个定时器
```

#### 初始化

&emsp;&emsp;`timer.init(ticks=, type=, callback=)`。

&emsp;&emsp;函数说明：初始化定时器`Timer`

- `ticks` 定时器执行的周期，单位是`10ms`， 隔period ms 执行一次。 period取值范围： `0 < period <= 3435973836`
- `type` 定时器的执行模式,可以是`Timer.PERIODIC`或`Timer.ONCE`
- `callback`： 定时器的回调函数，传入的一个参数是`timer`
  - 在callback函数里面传入其他参数，可以采用**Lambda表达式** 的方法。
  - 注意，**不要在定时器回调函数里面创建变量**，可以使用 **全局变量 global**，因为定时器每次都创建变量比较消耗内存。

&emsp;&emsp;示例:

```python
>>> from machine import Timer
>>> timer = Timer(1)#创建一个定时器
>>> timer.init(period=200, mode=Timer.PERIODIC , callback=lambda t:print("callback"))#初始化定时器，隔2s执行回调函数打印callback，一直执行
```

#### 释放资源

&emsp;&emsp;`timer.deinit()`。

&emsp;&emsp;函数说明：释放定时器`Timer`资源

&emsp;&emsp;定时器使用完了记得要释放定时器资源，键盘中断并不会销毁定时器，定时器会一直产生回调函数。

```python
>>> from machine import Timer
>>> timer = Timer(1)#创建一个定时器
>>> timer.init(period=2000, mode=Timer.PERIODIC , callback=lambda t:print("callback"))#初始化定时器，隔2s执行回调函数打印callback，一直执行
>>> timer.deinit()#释放资源
```

#### 查看状态

&emsp;&emsp;`timer.isrunning()`。

&emsp;&emsp;函数说明：查看定时器`Timer`是否在运行，返回运行状态，True正在运行，False不在运行。

&emsp;&emsp;有时定时器调用回调函数时间可能比较长，或者没有回调函数，不方便判断现在定时器是否还在运行，可以采用该函数查看。

```python
>>> from machine import Timer
>>> timer = Timer(1)#创建一个定时器
>>> timer.init(period=2000, mode=Timer.PERIODIC , callback=lambda t:print("callback"))#初始化定时器，隔2s执行回调函数打印callback，一直执行
>>> timer.isrunning()#查看定时器运行状态
True
```

## 示例

&emsp;&emsp;定时打印文字

```python
from machine import Timer
def test(args):
    print("Test")
t = Timer(0)
t.init(type=Timer.PERIODIC, callback=test, ticks=3000)
t.deinit()
```

&emsp;&emsp;第一行导入`machine`模块的定时类`Timer`

&emsp;&emsp;第二、三行创建回调函数，打印Test

&emsp;&emsp;第四创建定时器0

&emsp;&emsp;第五行初始化定时器

- ticks=3000:定时器运行周期3s

- type=Timer.PERIODIC：定时器以通道的已配置频率3s定期运行
- callback=test:回调函数是创建的函数`test`

&emsp;&emsp;第六行释放定时器0资源。

